/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package threads;

import java.io.DataInputStream;
import java.net.ServerSocket;
import java.net.Socket;
import javax.swing.JTextArea;

/**
 *
 * @author Javier
 */
public class MyRunnableInterface implements Runnable{

     private JTextArea txtMessages;

    public MyRunnableInterface() {
        txtMessages=null;
    }
          
    public MyRunnableInterface(JTextArea txtMessages) {
        this.txtMessages = txtMessages;
    } 
     
    public JTextArea getTxtMessages() {
        return txtMessages;
    }

    public void setTxtMessages(JTextArea txtMessages) {
        this.txtMessages = txtMessages;
    }
     
     
    
    @Override
    public void run() {
        ServerSocket serverSocket;
        Socket server;
        try{
            serverSocket = new ServerSocket(5555);
            //System.out.println("Ya creó el ServerSocket");
            while (true) {                
                try {
                    server=serverSocket.accept();
                    //System.out.println("Ya aceptó algo del servidor");
                    DataInputStream inputStream = new DataInputStream(server.getInputStream());
                    txtMessages.setText(txtMessages.getText()+"\n"+inputStream.readUTF());
                    server.close();
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }
            }
            
        }catch(Exception ex){
            System.out.println(ex.getMessage());
        }
    }
    
}

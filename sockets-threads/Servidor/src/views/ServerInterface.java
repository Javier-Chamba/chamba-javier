/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package views;

import javax.swing.*;
import java.io.IOException;
import java.io.DataInputStream;
import java.net.ServerSocket;
import java.net.Socket;
import threads.MyRunnableInterface;

/**
 *
 * @author Javier
 */
public final class ServerInterface extends JFrame /*implements Runnable*/ {

    private final JLabel lblTitle, lblMessages;
    private final JTextArea txtMessages;
    private final JButton btnLimpiar;
    private final JPanel panel;
    //private final Thread thread;

    private MyThread myThread;

    public ServerInterface(String title) {
        lblTitle = new JLabel("Chat (Servidor)");
        lblMessages = new JLabel("Chat: ");
        txtMessages = new JTextArea(10, 25);
        btnLimpiar = new JButton("Limpiar");
        panel = new JPanel();

        panel.add(lblTitle);
        panel.add(lblMessages);
        panel.add(txtMessages);
        panel.add(btnLimpiar);

        this.setBounds(800, 350, 320, 410);
        this.add(panel);
        this.setVisible(true);
        this.setTitle(title);
        //listen();
        /*
        thread=new Thread(this);
        thread.start();
         */

 /*
         MyRunnableInterface myRunnable = new MyRunnableInterface(txtMessages);
            thread=new Thread(myRunnable);
            thread.start();
*/
         
 
        myThread = new MyThread(txtMessages);
        myThread.start();


    }

//    public void listen() {
//        ServerSocket serverSocket;
//        Socket server;
//        try{
//            serverSocket = new ServerSocket(5555);
//            //System.out.println("Ya creó el ServerSocket");
//            while (true) {                
//                try {
//                    server=serverSocket.accept();
//                    //System.out.println("Ya aceptó algo del servidor");
//                    DataInputStream inputStream = new DataInputStream(server.getInputStream());
//                    txtMessages.setText(txtMessages.getText()+"\n"+inputStream.readUTF());
//                    server.close();
//                } catch (Exception e) {
//                    System.out.println(e.getMessage());
//                }
//            }
//            
//        }catch(Exception ex){
//            System.out.println(ex.getMessage());
//        }
//    }
//
//    @Override
//    public void run() {
//        ServerSocket serverSocket;
//        Socket server;
//        try{
//            serverSocket = new ServerSocket(5555);
//            //System.out.println("Ya creó el ServerSocket");
//            while (true) {                
//                try {
//                    server=serverSocket.accept();
//                    //System.out.println("Ya aceptó algo del servidor");
//                    DataInputStream inputStream = new DataInputStream(server.getInputStream());
//                    txtMessages.setText(txtMessages.getText()+"\n"+inputStream.readUTF());
//                    server.close();
//                } catch (Exception e) {
//                    System.out.println(e.getMessage());
//                }
//            }
//            
//        }catch(Exception ex){
//            System.out.println(ex.getMessage());
//        }
//    }
}

class MyThread extends Thread {

    private JTextArea txtMessages;

    public MyThread(JTextArea txtMessages) {
        this.txtMessages = txtMessages;
    }

    public void setTxtMessages(JTextArea txtMessages) {
        this.txtMessages = txtMessages;
    }

    public JTextArea getTxtMessages() {
        return txtMessages;
    }

    @Override
    public void run() {
        ServerSocket serverSocket;
        Socket server;
        try {
            serverSocket = new ServerSocket(5555);
            //System.out.println("Ya creó el ServerSocket");
            while (true) {
                try {
                    server = serverSocket.accept();
                    //System.out.println("Ya aceptó algo el servidor");
                    DataInputStream inputStream = new DataInputStream(server.getInputStream());
                    txtMessages.setText(txtMessages.getText() + "\n" + inputStream.readUTF());
                    System.out.println(inputStream.readUTF());
                    server.close();
                } catch (IOException ex) {
                    System.out.println(ex.getMessage());
                }
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }
}

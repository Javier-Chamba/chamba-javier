/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serverSocket;

import javax.swing.JFrame;
import views.ServerInterface;

/**
 *
 * @author Javier
 */
public class Server {
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        ServerInterface interfaceServer= new ServerInterface("Chat - SocketServer Java");
        interfaceServer.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
    
}

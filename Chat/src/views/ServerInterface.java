/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package views;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import serverSocket.MyThread;

/**
 *
 * @author Javier
 */
public class ServerInterface extends JFrame{
     private final JLabel lblTitle, lblMessages; /*, lblMessage*/
    //private final JTextField txtClient, txtIp; //txtMessage
    private final JTextArea txtMessages;
    private final JButton btnLimpiar; //btnSubmit,
    private final JPanel panel;
    //private final Thread myThread;
    private MyThread myThread;
    
    public JTextArea getMenssages(){
        return txtMessages;
    }
    
    public ServerInterface(String title){
        lblTitle    = new JLabel("Chat (Servidor)");
        lblMessages = new JLabel ("Chat: ");
        txtMessages = new JTextArea (10, 25);
        btnLimpiar  = new JButton("Limpiar");
        panel       = new JPanel();

        panel.add(lblTitle);
        panel.add(lblMessages);  panel.add(txtMessages);
        panel.add(btnLimpiar);
        
        this.setBounds(800, 350, 320, 410);
        this.add(panel);
        this.setVisible(true);
        this.setTitle(title);
        /*listen();*/
        
        myThread = new MyThread ("Hilo", this);
        myThread.start();
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package views;

import client.Message;
import clientSocket.ClientMyThread;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

/**
 *
 * @author Javier
 */
public class ClientInterface extends JFrame{

    private final JLabel lblTitle, lblClient, lblIp, lblMessage, lblMessages;
    private final JTextField txtMessage, txtClient, txtIp;
    private final JTextArea txtMessages;
    private final JButton btnSubmit, btnLimpiar;
    private final JPanel panel;

    private ClientMyThread clientMyThread;

    public JTextArea getMenssages() {
        return txtMessages;
    }

    private Socket socketClient;

    public ClientInterface(String title) {
        lblTitle = new JLabel("Chat (Cliente)");
        lblClient = new JLabel("Nick: ");
        lblIp = new JLabel("Ip chat: ");
        lblMessage = new JLabel("Mensaje: ");
        lblMessages = new JLabel("-Chat:-");

        txtMessage = new JTextField(25);
        txtMessages = new JTextArea(10, 25);
        txtClient = new JTextField(25);
        txtIp = new JTextField(25);

        btnSubmit = new JButton("Enviar");
        btnLimpiar = new JButton("Limpiar");

        btnSubmit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    //System.out.println("Hola funciona");
                    socketClient = new Socket("192.168.1.9", 5555);
                    try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(socketClient.getOutputStream())) {
                        //outputStream.writeUTF(txtMessage.getText());
                        Message message = new Message(txtClient.getText(),
                                txtIp.getText(), txtMessage.getText());
                        txtMessages.append("\n" + txtClient.getText() + ": " + txtMessage.getText());
                        objectOutputStream.writeObject(message);
                        //outputStream.close();
                        socketClient.close();
                    }
                } catch (IOException ex) {
                    txtMessages.setText(txtMessages.getText() + "\n" + txtMessage.getText() + " - Message not sent.");
                    System.out.println(ex.getMessage());
                }
            }
        });

        panel = new JPanel();

        panel.add(lblTitle);
        panel.add(lblIp);
        panel.add(txtIp);
        panel.add(lblClient);
        panel.add(txtClient);
        panel.add(lblMessages);
        panel.add(txtMessages);
        panel.add(lblMessage);
        panel.add(txtMessage);
        panel.add(btnSubmit);
        panel.add(btnLimpiar);

        this.setBounds(800, 350, 320, 410);
        this.add(panel);
        this.setVisible(true);
        this.setTitle(title);

        clientMyThread = new ClientMyThread("Hilo", this);
        clientMyThread.start();

    }

}

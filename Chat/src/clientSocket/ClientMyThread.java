/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clientSocket;

import client.Message;
import java.io.ObjectInputStream;
import java.net.ServerSocket;
import java.net.Socket;
import views.ClientInterface;

/**
 *
 * @author Javier
 */
public class ClientMyThread extends Thread{
    private ClientInterface clientInterface;
    
    public ClientMyThread(String name, ClientInterface clientInterfaz){
        super(name);
        this.clientInterface = clientInterfaz;
        //start();
    }

    @Override
    public void run() {
        ServerSocket clientSocket;
        Socket client;
        try {
            clientSocket = new ServerSocket(5555);
            Message chat;
            while (true) {
                client = clientSocket.accept();
                ObjectInputStream objectInputStream = new ObjectInputStream(client.getInputStream());
                chat = (Message) objectInputStream.readObject();
                clientInterface.getMenssages().append("\n"+chat.getNick() + ": " + chat.getTexto());
                client.close();
            }

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serverSocket;

import client.Message;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;
import views.ServerInterface;

/**
 *
 * @author Javier
 */
public class MyThread extends Thread{
    private ServerInterface serverInterface;
    
    public MyThread(String name, ServerInterface serverInterfaz){
        super(name);
        this.serverInterface = serverInterfaz;
        //start();
    }

    @Override
    public void run() {
        ServerSocket serverSocket;
        Socket server, socketChat;
        try {
            serverSocket = new ServerSocket(5555);
            
            Message receivedMessage;
            while (true)
            {
                try{
                    server = serverSocket.accept();
                    
                    //DataInputStream inputStream = new DataInputStream(server.getInputStream());
                    ObjectInputStream objectInputStream = new ObjectInputStream(server.getInputStream());
                    try {
                        receivedMessage = (Message) objectInputStream.readObject();
                        serverInterface.getMenssages().append("\nNick: " + receivedMessage.getNick() + " - Ip: " 
                                + receivedMessage.getIp() + " - Texto: " + receivedMessage.getTexto());
                        
                        socketChat=new Socket(receivedMessage.getIp(),5555);
                        ObjectOutputStream mensajeChat = new ObjectOutputStream(socketChat.getOutputStream());
                        mensajeChat.writeObject(receivedMessage);
                        
                        socketChat.close();
                        server.close();
                        
                    } catch (Exception ex) {
                        System.out.println(ex.getMessage());
                    }
                    
                    //serverInterface.getMenssages().setText(serverInterface.getMenssages().getText() + "\n" + inputStream.readUTF());
                    
                }
                catch (IOException ex){
                    System.out.println (ex.getMessage());
                }
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clientSocket;

import client.Message;
import java.io.ObjectInputStream;
import java.net.ServerSocket;
import java.net.Socket;
import views.ClientInterface;

/**
 *
 * @author Javier
 */
public class ClientMyThread extends Thread{
    private ClientInterface clientInterface;
    
    public ClientMyThread(String name, ClientInterface clientInterfaz){
        super(name);
        this.clientInterface = clientInterfaz;
        //start();
    }

    //Utilizo un hilo para el cliente me esté escuchando los mensajes de los otros clientes del sockets
    
    @Override
    public void run() {
        ServerSocket clientSocket;
        Socket client;
        try {
            clientSocket = new ServerSocket(9999);
            Message chat;
            while (true) {
                client = clientSocket.accept();
                //Flujo de datos de entrada para receptar el flujo de datos del servidor al cliente
                ObjectInputStream objectInputStream = new ObjectInputStream(client.getInputStream());
                //Leer los datos del objectInputStream y los convierte en una clase Message
                chat = (Message) objectInputStream.readObject();
                //Ingresa los valores al txtMessages, solamente el nick y texto
                clientInterface.getMenssages().append("\n"+chat.getNick() + ": " + chat.getTexto());
                client.close(); //Cierro el socket
            }

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }
}

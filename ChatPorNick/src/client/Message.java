/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package client;

import java.io.Serializable;

/**
 *
 * @author Javier
 */
public class Message implements Serializable{
    
    //Creación de variables String para el envío y recepción de datos
    private String nick, receptor, texto;

    //Contructor para inicializar los variables
    public Message(String nick, String receptor, String texto) {
        this.nick = nick;
        this.receptor = receptor;
        this.texto = texto;
    }

    public Message() {
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public String getReceptor() {
        return receptor;
    }

    public void setReceptor(String receptor) {
        this.receptor = receptor;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }
    
}

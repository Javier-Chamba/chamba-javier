/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package views;

import client.Message;
import clientSocket.ClientMyThread;
import com.sun.org.apache.xml.internal.security.encryption.XMLCipher;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;
import javafx.event.Event;
import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

/**
 *
 * @author Javier
 */
public class ClientInterface extends JFrame{

    private final JLabel lblTitle, lblEmisor, lblReceptor, lblMessage, lblMessages;
    private final JTextField txtMessage, txtEmisor, txtReceptor;
    private final JTextArea txtMessages;
    private final JButton btnSubmit, btnLimpiar;
    private final JPanel panel;
    String nick="";

    private ClientMyThread clientMyThread;

    public JTextArea getMenssages() {
        return txtMessages;
    }

    private Socket socketClient;

    public ClientInterface(String title) {
        //Ingresar el nick al iniciar la aplicación
        nick = JOptionPane.showInputDialog("Ingrese su nick");
        
        //Creación de las cajas de textos en el formulario
        lblTitle = new JLabel("Chat (Cliente)");
        lblEmisor = new JLabel("Nick: ");
        lblReceptor = new JLabel("Receptor chat: ");
        lblMessage = new JLabel("Mensaje: ");
        lblMessages = new JLabel("-Chat:-");

        txtMessage = new JTextField(25);
        txtMessages = new JTextArea(10, 25);
        txtEmisor = new JTextField(25);
        //El valor ingresado del nick, enviado a la caja de txtEmisor
        txtEmisor.setText(nick);
        txtReceptor = new JTextField(25);

        btnSubmit = new JButton("Enviar");
        btnLimpiar = new JButton("Limpiar");
        
        if (!nick.equals("")) {
            try {
                    socketClient = new Socket("192.168.1.9", 5555); //Crea el socket con la ip y puerto del servidor
                    //Flujo de datos para enviar los datos desde el cliente al servidor
                    try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(socketClient.getOutputStream())) {
                        Message message = new Message(txtEmisor.getText(),null,null); //Escriba los datos del cliente a la clase de Message
                        objectOutputStream.writeObject(message); //Escriba el flujo de datos del mensaje
                        socketClient.close(); //Cierro el sockets
                    }
                } catch (IOException ex) {
                    System.out.println(ex.getMessage());
                }
        }
        
        //Envío de datos al socket del servidor con el uso del botón Enviar

        btnSubmit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    //System.out.println("Hola funciona");
                    socketClient = new Socket("192.168.1.9", 5555); //Crea el socket con la ip y puerto del servidor
                    //Flujo de datos para enviar los datos desde el cliente al servidor
                    try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(socketClient.getOutputStream())) {
                        //outputStream.writeUTF(txtMessage.getText());
                        //Escriba los datos del cliente a la clase de Message
                        Message message = new Message(txtEmisor.getText(),
                                txtReceptor.getText(), txtMessage.getText());
                        //Escriba en el JTextArea txtMessages los datos del emisor y el mensaje
                        txtMessages.append("\n" + txtEmisor.getText() + ": " + txtMessage.getText());
                        objectOutputStream.writeObject(message); //Escriba el flujo de datos del mensaje
                        socketClient.close(); //Cierro el sockets
                    }
                } catch (IOException ex) {
                    txtMessages.setText(txtMessages.getText() + "\n" + txtMessage.getText() + " - Message not sent.");
                    System.out.println(ex.getMessage());
                }
            }
        });

        panel = new JPanel();

        panel.add(lblTitle);
        panel.add(lblReceptor);
        panel.add(txtReceptor);
        panel.add(lblEmisor);
        panel.add(txtEmisor);
        panel.add(lblMessages);
        panel.add(txtMessages);
        panel.add(lblMessage);
        panel.add(txtMessage);
        panel.add(btnSubmit);
        panel.add(btnLimpiar);

        this.setBounds(800, 350, 320, 410);
        this.add(panel);
        this.setVisible(true);
        this.setTitle(title);

        clientMyThread = new ClientMyThread("Hilo", this);
        clientMyThread.start();

    }

}

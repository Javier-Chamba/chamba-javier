/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serverSocket;

import client.Message;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import views.ServerInterface;

/**
 *
 * @author Javier
 */
public class MyThread extends Thread {

    private ServerInterface serverInterface;

    public MyThread(String name, ServerInterface serverInterfaz) {
        super(name);
        this.serverInterface = serverInterfaz;
        //start();
    }

    @Override
    public void run() {
        ServerSocket serverSocket;
        Socket server, socketChat;
        try {
            serverSocket = new ServerSocket(5555);

            //Creo una hashMap para guardar las Ip con sus respectivos nicks
            HashMap<String, String> hashMap = new HashMap<String, String>();

            Message receivedMessage;
            while (true) {
                try {
                    server = serverSocket.accept();

                    //DataInputStream inputStream = new DataInputStream(server.getInputStream());
                    //Flujo de datos de entrada para receptar el flujo de datos del cliente
                    ObjectInputStream objectInputStream = new ObjectInputStream(server.getInputStream());
                    try {
                        receivedMessage = (Message) objectInputStream.readObject();
                        //En caso que no hay receptor, no visualiza nada
                        if (receivedMessage.getReceptor() != null) {
                            //Ingresa en el JTextArea de txtMessages los valores de: nick, receptor y texto.
                            serverInterface.getMenssages().append("\nNick: " + receivedMessage.getNick() + " - Receptor: "
                                    + receivedMessage.getReceptor() + " - Texto: " + receivedMessage.getTexto());
                        }

                        //Obtengo la Ip del cliente
                        InetAddress IPv4 = server.getInetAddress();
                        //La convierto a cadena
                        String ipv4 = IPv4.getHostAddress();
                        //Guardo la ip con el nick en el hashMap
                        hashMap.put(ipv4, receivedMessage.getNick());

                        //System.out.println(hashMap.entrySet());

                        //Corro los valores uno por uno del hashMap
                        for (Map.Entry<String, String> entry : hashMap.entrySet()) {
                            String key = entry.getKey(); //Obtener la clave de la ip
                            String value = entry.getValue(); //Obtener el valor del nick
                            //Pregunto si el receptor se encuentra almacenado en el hashMap
                            if (value.equals(receivedMessage.getReceptor())) {
                                socketChat = new Socket(key, 9999);//Si se encuentra, obtengo la clave de la ip y el puerto para el envío al cliente
                                ObjectOutputStream mensajeChat = new ObjectOutputStream(socketChat.getOutputStream()); //Obtengo el flujo de dato del socket
                                mensajeChat.writeObject(receivedMessage); //Escribo los mensajes del chat
                                socketChat.close(); //Cierro el sockets

                            }

                        }
                        server.close(); //Cierro el sockets

                    } catch (Exception ex) {
                        System.out.println(ex.getMessage());
                    }

                    //serverInterface.getMenssages().setText(serverInterface.getMenssages().getText() + "\n" + inputStream.readUTF());
                } catch (IOException ex) {
                    System.out.println(ex.getMessage());
                }
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }
}
